<?php
script('hca_app', 'main');
script('hca_app', 'templates');
style('hca_app', 'style');
?>

<div id="app" class="hca-app">
    <div id="app-navigation">
        <ul>
            <li><a id="nav-new-request">New Resource Requests</a></li>
            <li><a id="nav-deprovisioning-request">Deprovisioning Requests</a></li>
            <li><a id="nav-request-history">Request History</a></li>
        </ul>
        <div class="loading-div"></div>
        <?php
        if (!function_exists('curl_version')) {
            echo "<span style='color:red;font-weight:bold'>Please install php-curl !</span>";
        }
        ?>
        <div id="app-settings">
            <div id="app-settings-header">
                <button class="settings-button" data-apps-slide-toggle="#app-settings-content"></button>
            </div>
            <div id="app-settings-content">
                <form id="hca-app-setting-form" >
                    <div>
                        <label for="hca-address" />HCA API URL</label>
                        <input id="hca-address" name="hcaUrl" type="text" />
                        <label for="hca-user" />HCA API User</label>
                        <input id="hca-user" name="hcaUser" type="text" />
                        <label for="hca-secret" />HCA API Secret</label>
                        <input id="hca-secret" name="hcaSecret" type="password" />
                    </div>
                    <button class="button" type="submit" />Save</button>
                    <span id="setting-save-success" style="display: none">✔️</span>
                    <button id="setting-test-hca-btn"  class="button" type="button" />Test API Connection</button>
                    <span id="setting-test-hca-btn-result-ok" style="display: none">✔️</span>
                    <span id="setting-test-hca-btn-result-failed" style="display: none">❌️</span>
                    <div style="font-size: 0.9em"><i>Save before testing connection</i></div>
                </form>
            </div>
        </div>

    </div>
    <div id="app-content">
        <div class="app-main-content">
            &nbsp;
        </div>
    </div>
</div>