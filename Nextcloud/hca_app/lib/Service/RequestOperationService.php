<?php

namespace OCA\HcaApp\Service;

use OCP\IGroupManager;
use OCA\GroupFolders\Folder\FolderManager;
use OCP\IUserManager;

class RequestOperationService {

    const GROUP_APPENDING = "#login_helmholtz";
    const QUOTA_MULTIPLIER = 1024 * 1024 * 1024;

    public $groupManager;
    public $groupFolderManager;
    public $userManager;

    function __construct(IGroupManager $groupManager, FolderManager $groupFolderManager, IUserManager $userManager) {
        $this->groupManager = $groupManager;
        $this->groupFolderManager = $groupFolderManager;
        $this->userManager = $userManager;
    }

    public function createGroupFolder($folderName) {
        return $this->groupFolderManager->createFolder($folderName);
    }

    public function setGroupFolderQuota($folderId, $quota) {
        return $this->groupFolderManager->setFolderQuota($folderId, $quota * self::QUOTA_MULTIPLIER);
    }

    public function addVoToGroupFolder($folderId, $vo) {
        $groupName = $vo . self::GROUP_APPENDING;
        if (!$this->groupManager->groupExists($groupName)) {
            $group = $this->groupManager->createGroup($groupName);
        } else {
            $group = $this->groupManager->get($groupName);
        }
        $this->groupFolderManager->addApplicableGroup($folderId, $group->getGid());
        if (substr_compare(strtolower($vo), "_read", -5, 5) === 0) {
            $this->groupFolderManager->setGroupPermissions($folderId, $group->getGid(), 1);
        }
    }

    public function removeVoFromGroupFolder($folderId, $vo) {
        $groupName = $vo . self::GROUP_APPENDING;
        $group = $this->groupManager->get($groupName);
        $this->groupFolderManager->removeApplicableGroup($folderId, $group->getGid());
    }

    public function renameGroupFolder($folderId, $folderName) {
        $this->groupFolderManager->renameFolder($folderId, $folderName);
    }

    public function removeGroupFolder($folderId) {
        $this->groupFolderManager->removeFolder($folderId);
    }

    public function removeUser($userId) {
        if ($this->userManager->userExists($userId)) {
            $user = $this->userManager->get($userId);
            if ($user->delete()) {
                return 0;
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

}
