<?php

namespace OCA\HcaApp\Service;

use OCA\HcaApp\Db\Request;
use OCA\HcaApp\Db\RequestMapper;
use OCA\HcaApp\Db\Resource;
use OCA\HcaApp\Db\ResourceMapper;
use OCA\HcaApp\Db\RequestOperation;
use OCA\HcaApp\Db\RequestOperationMapper;
use OCA\HcaApp\Service\HcaResponseService;
use OCA\HcaApp\Service\RequestOperationService;

class RequestService {

    public $requestMapper;
    public $resourceMapper;
    public $requestOperationMapper;
    public $requestOperationService;
    public $hcaResponse;

    function __construct(RequestMapper $requestMapper, ResourceMapper $resourceMapper, RequestOperationMapper $requestOperationMapper, RequestOperationService $requestOperationService, HcaResponseService $hcaResponse) {
        $this->requestMapper = $requestMapper;
        $this->resourceMapper = $resourceMapper;
        $this->requestOperationMapper = $requestOperationMapper;
        $this->requestOperationService = $requestOperationService;
        $this->hcaResponse = $hcaResponse;
    }

    public function createGroupStorageRequest($correlationId, $vos, $specification) {
        $req = new Request();
        $req->setRequestId($correlationId);
        $req->setType(Request::TYPE_RESOURCE_ALLOCATE);
        $req->setStatus(Request::STATUS_NEW);
        $req->setDate(date("Y-m-d H:i:s"));
        $req = $this->requestMapper->insert($req);

        $res = new Resource();
        $res->setType(Resource::TYPE_GROUP_STORAGE);
        $res = $this->resourceMapper->insert($res);
        $this->requestMapper->updateResourceId($req->getId(), $res->getId());

        $roGrp = new RequestOperation();
        $roGrp->setType(RequestOperation::TYPE_CREATE_GRP_FOLDER);
        $roGrp->setValue($specification["desiredName"]);
        $roGrp = $this->requestOperationMapper->insert($roGrp);
        $this->requestOperationMapper->updateRequestId($roGrp->getId(), $req->getId());
        $roQuo = new RequestOperation();
        $roQuo->setType(RequestOperation::TYPE_SET_QUOTA);
        $roQuo->setValue($this->hcaQuotaToGbValue($specification["quota"]));
        $roQuo = $this->requestOperationMapper->insert($roQuo);
        $this->requestOperationMapper->updateRequestId($roQuo->getId(), $req->getId());
        foreach ($vos as $vo) {
            $roVo = new RequestOperation();
            $roVo->setType(RequestOperation::TYPE_ADD_VO);
            $roVo->setValue($vo);
            $roVo = $this->requestOperationMapper->insert($roVo);
            $this->requestOperationMapper->updateRequestId($roVo->getId(), $req->getId());
        }
        return ($req instanceof Request);
    }

    public function updateGroupStorageRequest($correlationId, $vos, $specification, $resourceId) {
        $res = $this->resourceMapper->getResource($resourceId);
        if (!$res instanceof Resource || $res->internalResourceId == null) {
            return false;
        }

        $req = new Request();
        $req->setRequestId($correlationId);
        $req->setType(Request::TYPE_RESOURCE_UPDATE);
        $req->setStatus(Request::STATUS_NEW);
        $req->setDate(date("Y-m-d H:i:s"));
        $req = $this->requestMapper->insert($req);
        $this->requestMapper->updateResourceId($req->getId(), $res->getId());

        $groupFolders = $this->requestOperationService->groupFolderManager->getAllFolders();
        $groupFolder = $groupFolders[$res->getInternalResourceId()];
        if ($groupFolder["mount_point"] !== $specification["desiredName"]) {
            $roRn = new RequestOperation();
            $roRn->setType(RequestOperation::TYPE_RENAME_GRP_FOLDER);
            $roRn->setValue($specification["desiredName"]);
            $roRn = $this->requestOperationMapper->insert($roRn);
            $this->requestOperationMapper->updateRequestId($roRn->getId(), $req->getId());
        }
        if ($groupFolder["quota"] / RequestOperationService::QUOTA_MULTIPLIER != $specification["quota"]["value"]) {
            $roQuo = new RequestOperation();
            $roQuo->setType(RequestOperation::TYPE_SET_QUOTA);
            $roQuo->setValue($this->hcaQuotaToGbValue($specification["quota"]));
            $roQuo = $this->requestOperationMapper->insert($roQuo);
            $this->requestOperationMapper->updateRequestId($roQuo->getId(), $req->getId());
        }
        foreach ($vos as $vo) {
            $groupName = $vo . RequestOperationService::GROUP_APPENDING;
            if (!array_key_exists($groupName, $groupFolder["groups"])) {
                $roVo = new RequestOperation();
                $roVo->setType(RequestOperation::TYPE_ADD_VO);
                $roVo->setValue($vo);
                $roVo = $this->requestOperationMapper->insert($roVo);
                $this->requestOperationMapper->updateRequestId($roVo->getId(), $req->getId());
            }
        }
        foreach ($groupFolder["groups"] as $key => $groupId) {
            $group = str_replace(RequestOperationService::GROUP_APPENDING, "", $key);
            if (!in_array($group, $vos)) {
                $roRemVo = new RequestOperation();
                $roRemVo->setType(RequestOperation::TYPE_REMOVE_VO);
                $roRemVo->setValue($group);
                $roRemVo = $this->requestOperationMapper->insert($roRemVo);
                $this->requestOperationMapper->updateRequestId($roRemVo->getId(), $req->getId());
            }
        }
        return true;
    }

    public function removeGroupStorageRequest($correlationId, $resourceId) {
        $res = $this->resourceMapper->getResource($resourceId);
        if (!$res instanceof Resource || $res->internalResourceId == null) {
            return false;
        }

        $req = new Request();
        $req->setRequestId($correlationId);
        $req->setType(Request::TYPE_RESOURCE_DEALLOCATE);
        $req->setStatus(Request::STATUS_NEW);
        $req->setDate(date("Y-m-d H:i:s"));
        $req = $this->requestMapper->insert($req);
        $this->requestMapper->updateResourceId($req->getId(), $res->getId());

        $roRem = new RequestOperation();
        $roRem->setType(RequestOperation::TYPE_REMOVE_GRP_FOLDER);
        $roRem->setValue($res->internalResourceId);
        $roRem = $this->requestOperationMapper->insert($roRem);
        return $this->requestOperationMapper->updateRequestId($roRem->getId(), $req->getId());
    }

    public function userDeprovisioningRequest($correlationId, $userId) {
        $req = new Request();
        $req->setRequestId($correlationId);
        $req->setType(Request::TYPE_USER_DEPROVISIONING);
        $req->setStatus(Request::STATUS_NEW);
        $req->setDate(date("Y-m-d H:i:s"));
        $req = $this->requestMapper->insert($req);
        /*
          $res = new Resource();
          $res->setType(Resource::TYPE_USER);
          $res->setInternalResourceId($userId);
          $res = $this->resourceMapper->insert($res);
          $this->requestMapper->updateResourceId($req->getId(), $res->getId());
         */
        $roRem = new RequestOperation();
        $roRem->setType(RequestOperation::TYPE_REMOVE_USER);
        $roRem->setValue($userId);
        $roRem = $this->requestOperationMapper->insert($roRem);
        return $this->requestOperationMapper->updateRequestId($roRem->getId(), $req->getId());
    }

    public function processRequestDecision(int $requestId, string $decision) {
        $req = $this->requestMapper->getRequest($requestId);
        if ($decision === "accept") {
            foreach ($req->operations as $op) {
                /* create folder not in switch case, cause no internal ID required */
                if ($op->type == RequestOperation::TYPE_CREATE_GRP_FOLDER) {
                    $folderId = $this->requestOperationService->createGroupFolder($op->value);
                    if (!is_int($folderId)) {
                        return false;
                    }
                    $this->resourceMapper->updateInternalResourceId($req->resource->id, $folderId);
                    $req->resource->internalResourceId = $folderId;
                } else {
                    if ($op->type != RequestOperation::TYPE_REMOVE_USER && empty($req->resource->internalResourceId)) {
                        return false;
                    }
                    switch ($op->type) {
                        case RequestOperation::TYPE_RENAME_GRP_FOLDER:
                            $this->requestOperationService->renameGroupFolder($req->resource->internalResourceId, $op->value);
                            break;
                        case RequestOperation::TYPE_SET_QUOTA:
                            $this->requestOperationService->setGroupFolderQuota($req->resource->internalResourceId, $op->value);
                            break;
                        case RequestOperation::TYPE_ADD_VO:
                            $this->requestOperationService->addVoToGroupFolder($req->resource->internalResourceId, $op->value);
                            break;
                        case RequestOperation::TYPE_REMOVE_VO:
                            $this->requestOperationService->removeVoFromGroupFolder($req->resource->internalResourceId, $op->value);
                            break;
                        case RequestOperation::TYPE_REMOVE_GRP_FOLDER:
                            $this->requestOperationService->removeGroupFolder($req->resource->internalResourceId);
                            break;
                        case RequestOperation::TYPE_REMOVE_USER:
                            $r = $this->requestOperationService->removeUser($op->value);
                            if ($r == 1) {
                                return ["success" => false, "message" => 'Error while trying to delete User: Unknown User "' . $op->value . '"'];
                            } elseif ($r == 2) {
                                return ["success" => false, "message" => "Error while trying to delete User: Execute removing function failed. Take a look at Nextcloud log."];
                            }
                            break;
                    }
                }
            }
            switch ($req->type) {
                case Request::TYPE_RESOURCE_ALLOCATE:
                    if ($this->hcaResponse->sendResourceCreated($req->requestId, $req->resource->id)) {
                        $r = $this->requestMapper->setRequestStatus($req->id, Request::STATUS_ACCEPTED);
                        return ["success" => $r, "message" => ""];
                    }
                    break;
                case Request::TYPE_RESOURCE_UPDATE:
                    if ($this->hcaResponse->sendResourceUpdated($req->requestId, $req->resource->id)) {
                        $r = $this->requestMapper->setRequestStatus($req->id, Request::STATUS_ACCEPTED);
                        return ["success" => $r, "message" => ""];
                    }
                    break;
                case Request::TYPE_RESOURCE_DEALLOCATE:
                    if ($this->hcaResponse->sendResourceDeallocated($req->requestId, $req->resource->id)) {
                        $r = $this->requestMapper->setRequestStatus($req->id, Request::STATUS_ACCEPTED);
                        return ["success" => $r, "message" => ""];
                    }
                    break;
                case Request::TYPE_USER_DEPROVISIONING:
                    if ($this->hcaResponse->sendUserDeprovisioned($req->requestId, $req->operations[0]->value)) {
                        $r = $this->requestMapper->setRequestStatus($req->id, Request::STATUS_ACCEPTED);
                        return ["success" => $r, "message" => ""];
                    }
                    break;
                default :
                    $r = $this->hcaResponse->requestHandlingNotImplemened($req->requestId, $req->resource->type);
                    return ["success" => $r, "message" => ""];
                    break;
            }
        } elseif ($decision === "reject") {
            if ($this->hcaResponse->sendRequestReject($req->requestId)) {
                $r = $this->requestMapper->setRequestStatus($req->id, Request::STATUS_REJECTED);
                $message = ($r ? "" : "Error while rejecting request. Take a look at Nextcloud log.");
                return ["success" => $r, "message" => $message];
            }
        }
        return ["success" => false, "message" => "Something went wrong. Please have a look whether Nextcloud processed operations already before trying again!"];
    }

    private function hcaQuotaToGbValue($hcaQuota) {
        $value = 1;
        switch ($hcaQuota["unit"]) {
            case "GB":
                $value = $hcaQuota["value"];
                break;
            case "TB":
                $value = $hcaQuota["value"] * 1024;
                break;
        }
        return $value;
    }

}
