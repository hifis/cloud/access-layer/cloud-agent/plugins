<?php

namespace OCA\HcaApp\Service;

use OCA\HcaApp\Service\SettingsService;
use OCA\HcaApp\Service\HttpService;

class HcaResponseService {

    private $appSettings;
    private $http;
    private $hcaUrl;

    function __construct(SettingsService $appSettings, HttpService $httpService) {
        $this->appSettings = $appSettings;
        $this->http = $httpService;
        $this->hcaUrl = $this->appSettings->getValue("hcaUrl");
        $hcaUser = $this->appSettings->getValue("hcaUser");
        $hcaSecret = $this->appSettings->getValue("hcaSecret");
        if (!empty($hcaUser) && !empty($hcaSecret)) {
            $this->http->setAuthentication("$hcaUser:$hcaSecret");
        }
    }

    public function sendResourceCreated($requestId, $resourceId) {
        $this->http->setUrl($this->hcaUrl . "/ResourceCreatedV1");
        $this->http->addHeader("content-type: application/json");
        $this->http->addHeader("messageId: $requestId");
        $payload = [
            "id" => $resourceId
        ];
        $this->http->setBody($payload);
        $r = $this->http->sendRequest(false);
        return ($r == "Created");
    }

    public function sendResourceUpdated($requestId, $resourceId) {
        $this->http->setUrl($this->hcaUrl . "/ResourceUpdatedV1");
        $this->http->addHeader("content-type: application/json");
        $this->http->addHeader("messageId: $requestId");
        $payload = [
            "id" => $resourceId
        ];
        $this->http->setBody($payload);
        $r = $this->http->sendRequest(false);
        return ($r == "Created");
    }

    public function sendResourceDeallocated($requestId, $resourceId) {
        $this->http->setUrl($this->hcaUrl . "/ResourceDeallocatedV1");
        $this->http->addHeader("content-type: application/json");
        $this->http->addHeader("messageId: $requestId");
        $payload = [
            "id" => $resourceId
        ];
        $this->http->setBody($payload);
        $r = $this->http->sendRequest(false);
        return ($r == "Created");
    }

    public function sendRequestReject($requestId) {
        $this->http->setUrl($this->hcaUrl . "/ErrorV1");
        $this->http->addHeader("content-type: application/json");
        $this->http->addHeader("messageId: $requestId");
        $payload = [
            "type" => "RequestRejected",
            "message" => "request rejected by administrator"
        ];
        $this->http->setBody($payload);
        $r = $this->http->sendRequest(false);
        return ($r == "Created");
    }

    public function sendUserDeprovisioned($requestId, $userId) {
        $this->http->setUrl($this->hcaUrl . "/UserDeprovisionedV1");
        $this->http->addHeader("content-type: application/json");
        $this->http->addHeader("messageId: $requestId");
        $payload = [
            "id" => $userId
        ];
        $this->http->setBody($payload);
        $r = $this->http->sendRequest(false);
        return ($r == "Created");
    }

    public function requestHandlingNotImplemened($requestId, $requestType) {
        $this->http->setUrl($this->hcaUrl . "/GenericResponseV1");
        $this->http->addHeader("content-type: application/json");
        $this->http->addHeader("messageId: $requestId");
        $payload = [
            "successful" => false,
            "message" => "HCA App could not handle request type $requestType. It's not implemened yet. :("
        ];
        $this->http->setBody($payload);
        $r = $this->http->sendRequest(false);
        return ($r == "Created");
    }

    public function pingHca() {
        $this->http->setUrl($this->hcaUrl . "/PingV1");
        $this->http->addHeader("content-type: application/json");
        $this->http->addHeader("messageId: nc_hca_ping_" . uniqid());
        $payload = [
            "message" => "Ping sent from Nextcloud HCA App"
        ];
        $this->http->setBody($payload);
        $r = $this->http->sendRequest(false);
        return ($r == "Created");
    }

}
