<?php

namespace OCA\HcaApp\Service;

use \OCP\IConfig;

class SettingsService {

    private $config;
    private $appName;

    public function __construct(IConfig $config, $appName) {
        $this->config = $config;
        $this->appName = $appName;
    }

    public function getValues() {
        $configs = [];
        $keys = $this->config->getAppKeys($this->appName);
        foreach ($keys as $key) {
            $value = $this->config->getAppValue($this->appName, $key);
            $config = [$key => $value];
            array_push($configs, $config);
        }
        return $configs;
    }

    public function getValue($key) {
        return $this->config->getAppValue($this->appName, $key);
    }

    public function setValue($key, $value) {
        $this->config->setAppValue($this->appName, $key, $value);
    }

}
