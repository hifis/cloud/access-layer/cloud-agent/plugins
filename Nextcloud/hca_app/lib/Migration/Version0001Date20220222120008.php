<?php

namespace OCA\HcaApp\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\SimpleMigrationStep;
use OCP\Migration\IOutput;

class Version0001Date20220222120008 extends SimpleMigrationStep {

    /**
     * @param IOutput $output
     * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
     * @param array $options
     * @return null|ISchemaWrapper
     */
    public function changeSchema(IOutput $output, Closure $schemaClosure, array $options) {
        /** @var ISchemaWrapper $schema */
        $schema = $schemaClosure();

        $resourceTable = null;
        $requestTable = null;
        $operationTable = null;

        /*
          $schema->dropTable('hca_request_operation');
          $schema->dropTable('hca_request');
          $schema->dropTable('hca_resource');
          return $schema;
         */

        /* HCA Resource Table */
        if (!$schema->hasTable('hca_resource')) {
            $resourceTable = $schema->createTable('hca_resource');
            $resourceTable->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true
            ]);
            $resourceTable->addColumn('internal_resource_id', 'string', [
                'notnull' => false,
                'length' => 64
            ]);
            $resourceTable->addColumn('type', 'smallint', [
                'notnull' => true,
                'length' => 4
            ]);
            $resourceTable->setPrimaryKey(['id']);
        } else {
            $resourceTable = $schema->getTable('hca_resource');
        }

        /* HCA Request Table */
        if (!$schema->hasTable('hca_request')) {
            $requestTable = $schema->createTable('hca_request');
            $requestTable->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true
            ]);
            $requestTable->addColumn('request_id', 'string', [
                'notnull' => true,
                'length' => 64
            ]);
            $requestTable->addColumn('f_hca_resource', 'integer', [
                'notnull' => false,
            ]);
            $requestTable->addColumn('type', 'integer', [
                'notnull' => true,
            ]);
            $requestTable->addColumn('status', 'smallint', [
                'notnull' => true,
                'length' => 4
            ]);
            $requestTable->addColumn('date', 'datetime', [
                'notnull' => true
            ]);

            $requestTable->setPrimaryKey(['id']);
            $requestTable->addForeignKeyConstraint($resourceTable, array('f_hca_resource'), array('id'));
        } else {
            $requestTable = $schema->getTable('hca_request');
        }


        /* HCA Request Operation Table */
        if (!$schema->hasTable('hca_request_operation')) {
            $operationTable = $schema->createTable('hca_request_operation');
            $operationTable->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true
            ]);
            $operationTable->addColumn('f_hca_request', 'integer', [
                'notnull' => false,
            ]);
            $operationTable->addColumn('type', 'smallint', [
                'notnull' => true,
                'length' => 4
            ]);
            $operationTable->addColumn('value', 'string', [
                'notnull' => true,
                'length' => 200
            ]);

            $operationTable->setPrimaryKey(['id']);
            $operationTable->addForeignKeyConstraint($requestTable, array('f_hca_request'), array('id'));
        } else {
            $operationTable = $schema->getTable('hca_request_operation');
        }

        return $schema;
    }

}
