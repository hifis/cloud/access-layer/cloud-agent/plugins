<?php

namespace OCA\HcaApp\Db;

use OCP\IDBConnection;
use OCP\AppFramework\Db\QBMapper;
use OCA\HcaApp\Db\Resource;

class ResourceMapper extends QBMapper {

    const TABLE_NAME_RESOURCE = "hca_resource";

    public function __construct(IDBConnection $db) {
        parent::__construct($db, self::TABLE_NAME_RESOURCE, Resource::class);
    }

    private function newResource($row) {
        $r = new Resource();
        $r->setId($row["id"]);
        $r->setInternalResourceId($row["internal_resource_id"]);
        $r->setType($row["type"]);
        return $r;
    }

    public function getResource($id) {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
                ->from(self::TABLE_NAME_RESOURCE)
                ->where($qb->expr()->eq('id', $qb->createNamedParameter($id)));
        $rows = $qb->execute()->fetchAll();
        if (!empty($rows)) {
            return $this->newResource($rows[0]);
        }
        return false;
    }

    public function updateInternalResourceId($id, $internalId) {
        $qb = $this->db->getQueryBuilder();
        $qb->update(self::TABLE_NAME_RESOURCE)
                ->set('internal_resource_id', $qb->createNamedParameter($internalId))
                ->where($qb->expr()->eq('id', $qb->createNamedParameter($id)));
        return $qb->execute();
    }

}
