<?php

namespace OCA\HcaApp\Db;

use OCP\IDBConnection;
use OCP\AppFramework\Db\QBMapper;
use OCA\HcaApp\Db\Resource;

class RequestOperationMapper extends QBMapper {

    const TABLE_NAME_REQUEST_OPERATION = "hca_request_operation";

    public function __construct(IDBConnection $db) {
        parent::__construct($db, self::TABLE_NAME_REQUEST_OPERATION, Resource::class);
    }

    public function updateRequestId($roId, $requestId) {
        $qb = $this->db->getQueryBuilder();
        $qb->update(self::TABLE_NAME_REQUEST_OPERATION)
                ->set('f_hca_request', $qb->createNamedParameter($requestId))
                ->where($qb->expr()->eq('id', $qb->createNamedParameter($roId)));
        return $qb->execute();
    }

    public function updateValue($roId, $value) {
        $qb = $this->db->getQueryBuilder();
        $qb->update(self::TABLE_NAME_REQUEST_OPERATION)
                ->set('value', $qb->createNamedParameter($value))
                ->where($qb->expr()->eq('id', $qb->createNamedParameter($roId)));
        return $qb->execute();
    }

    private function newRequestOperation($row) {
        $r = new RequestOperation();
        $r->setId($row["id"]);
        $r->setValue($row["value"]);
        $r->setType($row["type"]);
        return $r;
    }

    public function getRequestOperation($id) {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
                ->from(self::TABLE_NAME_REQUEST_OPERATION)
                ->where($qb->expr()->eq('id', $qb->createNamedParameter($id)));
        $row = $qb->execute()->fetchAll();
        if (!empty($row)) {
            return $this->newRequestOperation($row);
        }
        return false;
    }

    public function getRequestOperationByRequest($requestId) {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
                ->from(self::TABLE_NAME_REQUEST_OPERATION)
                ->where($qb->expr()->eq('f_hca_request', $qb->createNamedParameter($requestId)));
        $rows = $qb->execute()->fetchAll();
        $requestOperations = [];
        foreach ($rows as $row) {
            array_push($requestOperations, $this->newRequestOperation($row));
        }
        return $requestOperations;
    }

}
