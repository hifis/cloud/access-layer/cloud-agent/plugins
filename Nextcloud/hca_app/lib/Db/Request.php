<?php

namespace OCA\HcaApp\Db;

use OCP\AppFramework\Db\Entity;

class Request extends Entity {

    const STATUS_REJECTED = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_NEW = 2;
    const TYPE_RESOURCE_ALLOCATE = 10;
    const TYPE_RESOURCE_UPDATE = 20;
    const TYPE_RESOURCE_DEALLOCATE = 30;
    const TYPE_USER_DEPROVISIONING = 100;

    public $requestId;  // id for response from cloud portal
    public $type;
    public $status;
    public $date;
    public $resource;
    public $operations;

    public function __construct() {
        $this->addType('id', 'integer');
    }

}
