<?php

namespace OCA\HcaApp\Db;

use OCP\IDBConnection;
use OCP\AppFramework\Db\QBMapper;
use OCA\HcaApp\Db\Request;
use OCA\HcaApp\Db\ResourceMapper;
use OCA\HcaApp\Db\RequestOperationMapper;

class RequestMapper extends QBMapper {

    const TABLE_NAME_REQUEST = "hca_request";
    const TABLE_NAME_GROUP_FOLDERS = "group_folders";

    private $resourceMapper;
    private $requestOperationMapper;

    public function __construct(IDBConnection $db, ResourceMapper $resourceMapper, RequestOperationMapper $requestOperationMapper) {
        $this->resourceMapper = $resourceMapper;
        $this->requestOperationMapper = $requestOperationMapper;
        parent::__construct($db, self::TABLE_NAME_REQUEST, Request::class);
    }

    public function updateResourceId($requestId, $resourceId) {
        $qb = $this->db->getQueryBuilder();
        $qb->update(self::TABLE_NAME_REQUEST)
                ->set('f_hca_resource', $qb->createNamedParameter($resourceId))
                ->where($qb->expr()->eq('id', $qb->createNamedParameter($requestId)));
        return $qb->execute();
    }

    private function newRequest($row) {
        $r = new Request();
        $r->setId($row["id"]);
        $r->setRequestId($row["request_id"]);
        $r->setType($row["type"]);
        $r->setStatus($row["status"]);
        $r->setDate($row["date"]);
        $r->setResource($this->resourceMapper->getResource($row["f_hca_resource"]));
        $operations = $this->requestOperationMapper->getRequestOperationByRequest($row["id"]);
        // lower value operations needs to be done first, cause highers are depending on lowers
        usort($operations, function ($a, $b) {
            return ($a->type < $b->type) ? -1 : 1;
        });
        $r->setOperations($operations);
        return $r;
    }

    public function getRequest($id) {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
                ->from(self::TABLE_NAME_REQUEST)
                ->where($qb->expr()->eq('id', $qb->createNamedParameter($id)));
        $rows = $qb->execute()->fetchAll();
        if (!empty($rows)) {
            return $this->newRequest($rows[0]);
        }
        return false;
    }

    public function getRequestByCorrelationId($correlationId) {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
                ->from(self::TABLE_NAME_REQUEST)
                ->where($qb->expr()->eq('request_id', $qb->createNamedParameter($correlationId)));
        $rows = $qb->execute()->fetchAll();
        if (!empty($rows)) {
            return $this->newRequest($rows[0]);
        }
        return false;
    }

    public function getNewRequests() {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
                ->from(self::TABLE_NAME_REQUEST)
                ->where($qb->expr()->eq('status', $qb->createNamedParameter(Request::STATUS_NEW)));
        $rows = $qb->execute()->fetchAll();
        $requests = [];
        foreach ($rows as $row) {
            array_push($requests, $this->newRequest($row));
        }
        return $requests;
    }

    public function getNewRequestsByType($types) {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')->from(self::TABLE_NAME_REQUEST)
                ->where($qb->expr()->eq('status', $qb->createNamedParameter(Request::STATUS_NEW)));
        if (count($types) > 0) {
            $qb->andWhere($qb->expr()->in('type', $qb->createNamedParameter($types, \Doctrine\DBAL\Connection::PARAM_INT_ARRAY)));
        }
        $rows = $qb->execute()->fetchAll();
        $requests = [];
        foreach ($rows as $row) {
            array_push($requests, $this->newRequest($row));
        }
        return $requests;
    }

    public function getRequestHistory() {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
                ->from(self::TABLE_NAME_REQUEST)->orderBy('date', 'desc');
        $rows = $qb->execute()->fetchAll();
        $requests = [];
        foreach ($rows as $row) {
            array_push($requests, $this->newRequest($row));
        }
        return $requests;
    }

    public function setRequestStatus($id, $newStatus) {
        $qb = $this->db->getQueryBuilder();
        $qb->update(self::TABLE_NAME_REQUEST)
                ->set('status', $qb->createNamedParameter($newStatus))
                ->where($qb->expr()->eq('id', $qb->createNamedParameter($id)));
        return $qb->execute();
    }

    public function isGroupFolderExisting($name) {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
                ->from(self::TABLE_NAME_GROUP_FOLDERS)
                ->where($qb->expr()->eq('mount_point', $qb->createNamedParameter($name)));
        $rows = $qb->execute()->fetchAll();
        return (count($rows) > 0);
    }

    public function getGroupFolderByName($name) {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
                ->from(self::TABLE_NAME_GROUP_FOLDERS)
                ->where($qb->expr()->eq('mount_point', $qb->createNamedParameter($name)));
        $rows = $qb->execute()->fetchAll();
        return (count($rows) > 0 ? $rows[0] : []);
    }

}
