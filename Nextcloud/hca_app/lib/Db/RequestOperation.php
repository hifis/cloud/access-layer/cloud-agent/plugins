<?php

namespace OCA\HcaApp\Db;

use OCP\AppFramework\Db\Entity;

class RequestOperation extends Entity {

    const TYPE_CREATE_GRP_FOLDER = 10;
    const TYPE_RENAME_GRP_FOLDER = 15;
    const TYPE_SET_QUOTA = 20;
    const TYPE_ADD_VO = 30;
    const TYPE_REMOVE_VO = 31;
    const TYPE_REMOVE_GRP_FOLDER = 90;
    const TYPE_REMOVE_USER = 500;

    public $value;
    public $type;

    public function __construct() {
        $this->addType('id', 'integer');
    }

}
