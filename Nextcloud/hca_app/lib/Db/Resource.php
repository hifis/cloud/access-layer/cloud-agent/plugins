<?php

namespace OCA\HcaApp\Db;

use OCP\AppFramework\Db\Entity;

class Resource extends Entity {

    const TYPE_GROUP_STORAGE = 10;
    const TYPE_USER = 100;

    public $internalResourceId;
    public $type;

    public function __construct() {
        $this->addType('id', 'integer');
    }

}
