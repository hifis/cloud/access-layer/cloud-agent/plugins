<?php

namespace OCA\HcaApp\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Controller;
use OCA\HcaApp\Service\SettingsService;
use \OCA\HcaApp\Service\HcaResponseService;

class SettingsController extends Controller {

    private $userId;
    private $service;
    private $hcaResponseService;

    public function __construct($AppName, IRequest $request, SettingsService $service, HcaResponseService $hcaResponseService, $UserId) {
        parent::__construct($AppName, $request);
        $this->userId = $UserId;
        $this->service = $service;
        $this->hcaResponseService = $hcaResponseService;
    }

    /**
     *
     */
    public function getSettings() {
        return new DataResponse($this->service->getValues());
    }

    /**
     *
     */
    public function updateSettings(array $settings) {
        foreach ($settings as $key => $value) {
            $this->service->setValue($key, $value);
        }
        return new DataResponse(true);
    }

    /**
     * Test HCA API URL
     */
    public function pingHca() {
        return new DataResponse($this->hcaResponseService->pingHca());
    }

}
