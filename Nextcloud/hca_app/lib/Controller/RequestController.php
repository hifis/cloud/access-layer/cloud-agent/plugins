<?php

namespace OCA\HcaApp\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Controller;
use OCP\IGroupManager;
use OCA\HcaApp\Db\Request;
use OCA\HcaApp\Db\Resource;
use OCA\HcaApp\Service\RequestService;

class RequestController extends Controller {

    const HCA_API_GROUP = "hca_api";
    const HELMHOLTZ_AAI_LOGIN = "@login.helmholtz.de";

    private $userId;
    private $service;
    private $groupManager;

    public function __construct($AppName, IRequest $request, RequestService $requestService, IGroupManager $groupManager, $UserId) {
        parent::__construct($AppName, $request);
        $this->userId = $UserId;
        $this->service = $requestService;
        $this->groupManager = $groupManager;
        date_default_timezone_set('Europe/Berlin');
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function createGroupStorageRequest(array $targetEntity, array $specification, string $correlationId) {
        $userCheck = $this->checkIsUserValid();
        if ($userCheck instanceof DataResponse) {
            return $userCheck;
        }

        /* create request */
        if (is_array($specification) && isset($specification["desiredName"]) && isset($specification["quota"]) && !empty($correlationId) && is_array($targetEntity) && isset($targetEntity["specification"]["vos"])) {
            $requestCheck = $this->requestExists($correlationId);
            if ($requestCheck instanceof DataResponse) {
                return $requestCheck;
            }
            $vos = $targetEntity["specification"]["vos"];
            if (!empty($vos)) {
                if ($this->service->requestMapper->isGroupFolderExisting($specification["desiredName"])) {
                    return new DataResponse(["type" => "error", "message" => "group storage already exists"], 400);
                }
                if ($this->service->createGroupStorageRequest($correlationId, $vos, $specification)) {
                    return new DataResponse(["type" => "success", "message" => "group storage request created"]);
                } else {
                    return new DataResponse(["type" => "error", "message" => "failed to create group storage request"], 500);
                }
            } else {
                return new DataResponse(["type" => "error", "message" => "no virtual organizations given"], 400);
            }
        } else {
            return new DataResponse(["type" => "error", "message" => "expect GroupStorageResource object in body"], 400);
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function updateGroupStorageRequest(array $targetEntity, array $specification, string $correlationId, string $resourceId) {
        $userCheck = $this->checkIsUserValid();
        if ($userCheck instanceof DataResponse) {
            return $userCheck;
        }

        /* update request */
        if (is_array($specification) && isset($specification["desiredName"]) && isset($specification["quota"]) && !empty($correlationId) && is_array($targetEntity) && isset($targetEntity["specification"]["vos"]) && !empty($resourceId)) {
            $requestCheck = $this->requestExists($correlationId);
            if ($requestCheck instanceof DataResponse) {
                return $requestCheck;
            }
            $vos = $targetEntity["specification"]["vos"];
            if (!empty($vos)) {
                $res = $this->service->resourceMapper->getResource($resourceId);
                if (!$res instanceof Resource || $res->internalResourceId == null) {
                    return new DataResponse(["type" => "error", "message" => "cannot find resource with given id"], 400);
                }
                $groupFolder = $this->service->requestMapper->getGroupFolderByName($specification["desiredName"]);
                if (count($groupFolder) > 0 && $groupFolder["folder_id"] != $res->internalResourceId) {
                    return new DataResponse(["type" => "error", "message" => "desiredName already used by other group storage"], 400);
                }
                if ($this->service->updateGroupStorageRequest($correlationId, $vos, $specification, $resourceId)) {
                    return new DataResponse(["type" => "success", "message" => "update request for group storage created"]);
                } else {
                    return new DataResponse(["type" => "error", "message" => "failed to create update request for group storage"], 500);
                }
            } else {
                return new DataResponse(["type" => "error", "message" => "no virtual organizations given"], 400);
            }
        } else {
            return new DataResponse(["type" => "error", "message" => "expect GroupStorageResource object in body"], 400);
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function removeGroupStorageRequest(string $correlationId, string $resourceId) {
        $userCheck = $this->checkIsUserValid();
        if ($userCheck instanceof DataResponse) {
            return $userCheck;
        }

        /* deallocation request */
        if (!empty($correlationId) && !empty($resourceId)) {
            $requestCheck = $this->requestExists($correlationId);
            if ($requestCheck instanceof DataResponse) {
                return $requestCheck;
            }
            $res = $this->service->resourceMapper->getResource($resourceId);
            if (!$res instanceof Resource || $res->internalResourceId == null) {
                return new DataResponse(["type" => "error", "message" => "cannot find resource with given id"], 400);
            }
            if ($this->service->removeGroupStorageRequest($correlationId, $resourceId)) {
                return new DataResponse(["type" => "success", "message" => "group storage deallocation request created"]);
            } else {
                return new DataResponse(["type" => "error", "message" => "failed to create deallocation request for group storage"], 500);
            }
        } else {
            return new DataResponse(["type" => "error", "message" => "expect correlation and resource id in body"], 400);
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function userDeprovisioningRequest(string $correlationId, string $userId) {
        $userCheck = $this->checkIsUserValid();
        if ($userCheck instanceof DataResponse) {
            return $userCheck;
        }

        /* deprovisioning request */
        if (!empty($correlationId) && !empty($userId)) {
            $requestCheck = $this->requestExists($correlationId);
            if ($requestCheck instanceof DataResponse) {
                return $requestCheck;
            }
            if (!$this->service->requestOperationService->userManager->userExists($userId)) {
                $userNotFoundResponse = ["type" => "error", "message" => "cannot find user with given id"];
                if (substr_count($userId, '-') == 4) {
                    $otherUserIdFormat = str_replace('-', '', $userId) . self::HELMHOLTZ_AAI_LOGIN;
                    if ($this->service->requestOperationService->userManager->userExists($otherUserIdFormat)) {
                        $userId = $otherUserIdFormat;
                    } else {
                        return new DataResponse($userNotFoundResponse, 400);
                    }
                } else {
                    return new DataResponse($userNotFoundResponse, 400);
                }
            }
            if ($this->service->userDeprovisioningRequest($correlationId, $userId)) {
                return new DataResponse(["type" => "success", "message" => "user deprovisioning request created"]);
            } else {
                return new DataResponse(["type" => "error", "message" => "failed to create user deprovisioning request"], 500);
            }
        } else {
            return new DataResponse(["type" => "error", "message" => "expect correlation and user id in body"], 400);
        }
    }

    private function checkIsUserValid() {
        $hcaGroup = $this->groupManager->get(self::HCA_API_GROUP);
        /* check group exists */
        if ($hcaGroup === null) {
            return new DataResponse(
                    ["type" => "error", "message" => "HCA group missing! Please add the group '" . self::HCA_API_GROUP . "' to nextcloud."],
                    500
            );
        }
        /* check user is in group */
        if (!array_key_exists($this->userId, $hcaGroup->getUsers())) {
            return new DataResponse(["type" => "error", "message" => "You are not allowed to access this resource."], 403);
        }
    }

    private function requestExists($correlation_id) {
        if ($this->service->requestMapper->getRequestByCorrelationId($correlation_id) instanceof Request) {
            return new DataResponse(["type" => "error", "message" => "request already exists.", "correlation_id" => $correlation_id], 400);
        }
    }

    /* #########                  ######### *
     * API endpoints for internal app usage  *
     * #########                  ######### *
     */

    /**
     *
     */
    public function getNewRequests() {
        $rq = $this->service->requestMapper->getNewRequests();
        return new DataResponse($rq);
    }

    /**
     * 
     */
    public function getNewRequestsByType($types) {
        if (empty($types)) {
            $types = [];
        }
        $rq = $this->service->requestMapper->getNewRequestsByType($types);
        return new DataResponse($rq);
    }

    /**
     *
     */
    public function getRequestHistory() {
        $rq = $this->service->requestMapper->getRequestHistory();
        return new DataResponse($rq);
    }

    /**
     *
     */
    public function setRequestDecision(int $id, string $type) {
        return new DataResponse($this->service->processRequestDecision($id, $type));
    }

    /**
     *
     */
    public function updateOperationValue(int $id, string $value) {
        return new DataResponse($this->service->requestOperationMapper->updateValue($id, $value));
    }

}
