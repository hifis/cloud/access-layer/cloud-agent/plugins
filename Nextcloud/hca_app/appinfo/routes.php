<?php

/**
 * Create your routes in here. The name is the lowercase name of the controller
 * without the controller part, the stuff after the hash is the method.
 * e.g. page#index -> OCA\HcaApp\Controller\PageController->index()
 *
 * The controller class has to be registered in the application.php file since
 * it's instantiated in there
 */
return [
    'routes' => [
        ['name' => 'page#index', 'url' => '/', 'verb' => 'GET'],
        ['name' => 'request#createGroupStorageRequest', 'url' => '/createGroupStorageRequest', 'verb' => 'POST'],
        ['name' => 'request#updateGroupStorageRequest', 'url' => '/updateGroupStorageRequest', 'verb' => 'POST'],
        ['name' => 'request#removeGroupStorageRequest', 'url' => '/removeGroupStorageRequest', 'verb' => 'POST'],
        ['name' => 'request#userDeprovisioningRequest', 'url' => '/userDeprovisioningRequest', 'verb' => 'POST'],
        ['name' => 'request#getNewRequests', 'url' => '/getNewRequests', 'verb' => 'GET'],
        ['name' => 'request#getNewRequestsByType', 'url' => '/getNewRequestsByType', 'verb' => 'POST'],
        ['name' => 'request#getRequestHistory', 'url' => '/getRequestHistory', 'verb' => 'GET'],
        ['name' => 'request#setRequestDecision', 'url' => '/setRequestDecision', 'verb' => 'POST'],
        ['name' => 'request#updateOperationValue', 'url' => '/updateOperationValue', 'verb' => 'POST'],
        ['name' => 'settings#getSettings', 'url' => '/getSettings', 'verb' => 'GET'],
        ['name' => 'settings#updateSettings', 'url' => '/updateSettings', 'verb' => 'POST'],
        ['name' => 'settings#pingHca', 'url' => '/pingHca', 'verb' => 'GET'],
    ]
];
