const templates = {
    "pages": {
        "newRequests": {
            "template": '<h2>New resource requests</h2><div id="new-request-container" class="default-table">\
                            <table><thead><tr><th>Request ID (HCA)</th><th>Date</th><th>Request Type</th><th>Resource Type</th><th>Resource ID</th><th>Operations</th><th>Options</th></tr></thead><tbody></tbody></table>\
                            <div class="overlay-container"><div class="overlay-edit-box"><h2>Change value</h2><div><input id="edit-value" type=text /></div><div class="btn-bar"><button id="edit-cancel" type="button" >Cancel</button><button id="edit-ok" type=button>OK</button></div></div></div>\
                        </div>',
            "exec": function () {
                getNewResourceRequests();
            }
        },
        "deprovisioningRequests": {
            "template": '<h2>New deprovisioning requests</h2><div id="new-request-container" class="default-table">\
                            <table><thead><tr><th>Request ID (HCA)</th><th>Date</th><th>Request Type</th><th>Operations</th><th>Options</th></tr></thead><tbody></tbody></table>\
                            <div class="overlay-container"><div class="overlay-edit-box"><h2>Change value</h2><div><input id="edit-value" type=text /></div><div class="btn-bar"><button id="edit-cancel" type="button" >Cancel</button><button id="edit-ok" type=button>OK</button></div></div></div>\
                        </div>',
            "exec": function () {
                getNewDeprovisioningRequests();
            }
        },
        "requestHistory": {
            "template": '<h2>Request history</h2><div id="request-history-container" class="default-table">\
                            <table><thead><tr><th>Request ID (HCA)</th><th>Date</th><th>Request Type</th><th>Resource Type</th><th>Resource ID</th><th>Operations</th><th>Status</th></tr></thead><tbody></tbody></table>\
                         </div>',
            "exec": function () {
                getRequestHistory();
            }
        },
    }
}