const REQUEST_STATUS_NAME = ["DENIED", "ACCEPTED", "NEW"];
const RESOURCE_TYPE = {
    "10": "GroupStorage",
    "100": "User"
};
const REQUEST_TYPE = {
    "10": "ResourceAllocate",
    "20": "ResourceUpdate",
    "30": "ResourceDeallocate",
    "100": "UserDeprovision"
};
const REQUEST_OPERATION_TYPE = {
    "10": "Create group folder",
    "15": "Rename group folder",
    "20": "Set Quota",
    "30": "Add VO",
    "31": "Remove VO",
    "90": "Remove group folder",
    "500": "Remove User"
};

var HCA_APP_CURRENT_PAGE = '';

$(document).ready(function () {
    loadPage('newRequests');
    $('#nav-new-request').on('click', function () {
        loadPage("newRequests");
    });
    $('#nav-deprovisioning-request').on('click', function () {
        loadPage("deprovisioningRequests");
    });
    $('#nav-request-history').on('click', function () {
        loadPage("requestHistory");
    });

    loadSettings();
    $("#hca-app-setting-form").submit(function (event) {
        event.preventDefault();
        saveSettings();
    });
    $('#setting-test-hca-btn').on('click', function () {
        testHcaApiConnection();
    });
});

/* new requests */
function getNewResourceRequests() {
    const data = {"types": [10, 20, 30]};
    httpRequest("POST", "getNewRequestsByType", data, function (response) {
        createNewRequestTable(response, true);
    });
}

function getNewDeprovisioningRequests() {
    const data = {"types": [100]};
    httpRequest("POST", "getNewRequestsByType", data, function (response) {
        createNewRequestTable(response, false);
    });
}

function createNewRequestTable(requests, isResource) {
    if (Array.isArray(requests)) {
        var html = '';
        for (var i = 0; i < requests.length; i++) {
            let resource = requests[i].resource.id;
            resource += (requests[i].resource.internalResourceId !== null ? '<br><b>Internal</b>: ' + requests[i].resource.internalResourceId : '');
            html += '<tr>';
            html += '  <td>' + requests[i].requestId + '</td>';
            html += '  <td>' + requests[i].date + '</td>';
            html += '  <td>' + REQUEST_TYPE[requests[i].type] + '</td>';
            if (isResource) {
                html += '  <td>' + RESOURCE_TYPE[requests[i].resource.type] + '</td>';
                html += '  <td>' + resource + '</td>';
            }
            html += '  <td>' + createRequestDetails(requests[i].operations, true) + '</td>';
            html += '  <td>';
            html += '    <button data-type="accept" data-request-id="' + requests[i].id + '" class="button" type="button" >accept</button> | ';
            html += '    <button data-type="reject" data-request-id="' + requests[i].id + '" class="button" type="button" >reject</button>';
            html += '  </td>';
            html += '</tr>';
        }
        $('#new-request-container table tbody').html(html);
        $('#new-request-container table tbody button').each(function () {
            $(this).on('click', function () {
                sendRequestDecision($(this).attr("data-request-id"), $(this).attr("data-type"));
            });
        });
        $('#new-request-container table tbody .request-operation').each(function () {
            $(this).on('click', function () {
                openRequestOperationEdit($(this).attr("data-operation-id"), $(this).attr("data-operation-value"));
            });
        });
    }
}


/* request history */
function getRequestHistory() {
    httpRequest("GET", "getRequestHistory", null, function (response) {
        createRequestHistory(response);
    });
}

function createRequestHistory(requests) {
    if (Array.isArray(requests)) {
        var html = '';
        for (var i = 0; i < requests.length; i++) {
            let resource = requests[i].resource.id;
            resource += (requests[i].resource.internalResourceId !== null ? '<br><b>Internal</b>: ' + requests[i].resource.internalResourceId : '');
            html += '<tr>';
            html += '  <td>' + requests[i].requestId + '</td>';
            html += '  <td>' + requests[i].date + '</td>';
            html += '  <td>' + REQUEST_TYPE[requests[i].type] + '</td>';
            if (requests[i].resource) {
                html += '  <td>' + RESOURCE_TYPE[requests[i].resource.type] + '</td>';
                html += '  <td>' + resource + '</td>';
            } else {
                html += '  <td>-</td>';
                html += '  <td>-</td>';
            }
            html += '  <td>' + createRequestDetails(requests[i].operations, false) + '</td>';
            html += '  <td>' + REQUEST_STATUS_NAME[requests[i].status] + '</td>';
            html += '</tr>';
        }
        $('#request-history-container table tbody').html(html);
    }
}

function createRequestDetails(operations, isLnk) {
    let html;
    if (operations.length == 0) {
        html = '<i>request contains no changes,<br>accept or reject</i>';
    } else {
        html = '<ul>';
        const lnkClass = (isLnk ? ' lnk' : '');
        for (var i = 0; i < operations.length; i++) {
            html += '<li class="request-operation' + lnkClass + '" data-operation-id="' + operations[i].id + '" data-operation-value="' + operations[i].value + '">' + REQUEST_OPERATION_TYPE[operations[i].type] + ': ' + operations[i].value + '</li>';
        }
        html += '</ul>';
    }
    return html;
}

function openRequestOperationEdit(id, value) {
    $(".overlay-container").css({"visibility": "visible", "opacity": "1"});
    $(".overlay-container #edit-value").val(value);
    $(".overlay-container #edit-ok").on('click', function () {
        sendNewRequestOperationValue(id, $(".overlay-container #edit-value").val());
    });
    $(".overlay-container #edit-cancel").on('click', function () {
        closeRequestOperationEdit();
    });
}

function closeRequestOperationEdit() {
    $(".overlay-container").css({"visibility": "hidden", "opacity": "0"});
}

function sendNewRequestOperationValue(id, value) {
    const payload = {
        "id": id,
        "value": value,
    };
    httpRequest("POST", "updateOperationValue", payload, function (response) {
        if (response) {
            loadPage('newRequests');
        } else {
            alert("An error occurred. Please try again.");
        }
    });
}

function sendRequestDecision(id, type) {
    const payload = {
        "id": id,
        "type": type,
    };
    httpRequest("POST", "setRequestDecision", payload, function (response) {
        if (response["success"]) {
            loadPage(HCA_APP_CURRENT_PAGE);
        } else {
            if (response["message"] !== '') {
                alert(response["message"]);
            } else {
                alert("An error occurred. Please have a look whether Nextcloud processed operations already before trying again!");
            }
        }
    });
}

/* settings */

function loadSettings() {
    httpRequest("GET", "getSettings", null, function (settings) {
        for (var i = 0; i < settings.length; i++) {
            for (var key in settings[i]) {
                $("#hca-app-setting-form input[name='" + key + "']").val(settings[i][key]);
            }
        }
    });
}

function saveSettings() {
    var payload = {
        "settings": {}
    };
    var data = $("#hca-app-setting-form").serializeArray();
    for (var i = 0; i < data.length; i++) {
        payload.settings[data[i].name] = data[i].value;
    }
    httpRequest("POST", "updateSettings", payload, function (r) {
        if (r) {
            $("#setting-save-success").fadeIn(function () {
                setTimeout(function () {
                    $("#setting-save-success").fadeOut();
                }, 2000);
            });
        }
    });
}

function testHcaApiConnection() {
    httpRequest("GET", "pingHca", null, function (r) {
        var resultEl = "#setting-test-hca-btn-result-failed";
        if (r) {
            resultEl = "#setting-test-hca-btn-result-ok";
        }
        $(resultEl).fadeIn(function () {
            setTimeout(function () {
                $(resultEl).fadeOut();
            }, 2000);
        });

    });
}

/* BASIC FUNCTIONS */

function loadPage(page) {
    HCA_APP_CURRENT_PAGE = page;
    $('.app-main-content').html(templates.pages[page].template);
    templates.pages[page].exec();
}

function isLoading(state) {
    if (state) {
        $('.loading-div').show();
    } else {
        $('.loading-div').hide();
    }
}

function httpRequest(method, apiArea, payload, callback) {
    isLoading(true);
    var request = {
        type: method,
        url: OC.generateUrl('/apps/hca_app/' + apiArea),
        success: function (response) {
            isLoading(false);
            callback(response);
        },
        error: function (error) {
            console.log(error);
            isLoading(false);
            alert("An error occurred. Please try again.");
        }
    };
    if (payload !== null) {
        request["data"] = JSON.stringify(payload);
        request["contentType"] = "application/json";
    }
    $.ajax(request);
}