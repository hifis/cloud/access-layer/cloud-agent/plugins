# HCA Plugin and Nextcloud HCA App

This Folder contains the HCA Plugin for the RabbitMQ service and a Nextcloud App. The HCA Plugin itself is acting like a proxy, it transmitted a request from the HCA Queue to the Nextcloud HCA App. The Nextcloud HCA App stores the request information and provides it displayed as a table for the administrator. The administrator has the option to accept or reject a request. He also gets an overview, which changes would be made, if he accepts a certain request. In case of accepting a request, the Nextcloud HCA App will make the changes in the nextcloud, for example creating a group folder, automatically. The App will also create a response for the Cloud Portal, in which it will call the HCA API of the RabbitMQ service.

## How to setup

### HCA Plugin

1. Get Helmholtz Cloud Agent as described here https://gitlab.hzdr.de/helmholtz-cloud-portal/local-agent/core
2. Copy HCA_Nextcloud Plugin folder (hca_plugin/plugins/HCA_Nextcloud) to the HCA Plugin folder
3. Prepare Nextcloud: Create a group named "hca_api". Add a local user (e.g. hcauser) and add the user to the group
4. Copy HCA Plugin Config (hca_plugin/configs/HCA_Nextcloud.json) to the HCA Config folder
5. Edit HCA Plugin Config: Set nc_base_url and enter credentials (nc_user, nc_password) of the user, which was created in step 3
6. Deploy and start Helmholtz Cloud Agent docker container
7. Optional: Setup reverse proxy for HCA API endpoints and create a basic auth configuration

### Nextcloud HCA APP

1. Copy the app files to the Nextcloud apps folder
2. Go to Nextcloud App management interface and verify that the App supports the installed Nextcloud Version
3. Install the App
4. Navigate to the HCA App which will appear in the app bar at the top (only accessible for administrators)
5. Open the app settings: Enter the HCA API URL with name of service (e.g. http://hca1-test.example.de/hca/nubes)
6. If you use a reverse proxy for the HCA API and basic authentication, you can enter the credentials for that in the fields HCA API User and HCA API Secret
7. Save configuration and test API connection with the test button
8. When pressing the test button: The Nexcloud HCA App will create a PingV1 payload. If the PingV1 was successfully queued in HCA, a green tick will appear. Please also check, whether the PingV1 receives to the RabbitMQ service of the Cloud Portal