import requests
from hca_messages import ResourceAllocateV1, ResourceUpdateV1, ResourceDeallocateV1, GenericResponseV1, \
    GroupStorageResourceSpecV1, TargetEntityV1, VOListSpecV1, UserDeprovisionV1
from logging import Logger
from typing import cast

SSL_CERT_CHECK = False


class Config:
    def __init__(self, nc_base_url: str, nc_user: str, nc_password: str):
        self.nc_base_url = nc_base_url
        self.nc_user = nc_user
        self.nc_password = nc_password


class GenericException(Exception):
    def __init__(self, message="An error occurred"):
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return self.message


class ResourceSpecTypeEmpty(Exception):
    def __init__(self, message="Resource spec type is empty. Request cannot be handled."):
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return self.message


class HCA_Nextcloud:
    __hca_config__ = Config

    def __init__(self, config, logger: Logger):
        self.config = config
        self.logger = logger
        self.requests_cookies = None

    def create_session(self, username, password):
        session = requests.Session()
        session.auth = (username, password)
        session.get(f"{self.config.nc_base_url}/login", verify=SSL_CERT_CHECK)
        self.requests_cookies = session.cookies

    def request_nc_hca(self, api_part, json_payload):
        self.logger.debug(f"JSON payload for Nextcloud HCA App: {json_payload}")
        r = requests.post(f"{self.config.nc_base_url}/index.php/apps/hca_app/{api_part}",
                          json=json_payload, cookies=self.requests_cookies, verify=SSL_CERT_CHECK)
        self.logger.debug(f"Response from Nextcloud HCA App:  {r.json()}")
        res = r.json()
        if res["type"] == "success":
            return GenericResponseV1(True, res["message"])
        if res["type"] == "error":
            raise GenericException(res["message"])
        return GenericException("transmitting request to Nextcloud HCA app failed")

    def resource_allocate_v1(self, props, payload: ResourceAllocateV1):
        if not payload.type:
            raise ResourceSpecTypeEmpty()
        self.logger.info(f"Provision resource of type '{payload.type}'")
        if payload.type == "GroupStorageResourceSpecV1":
            self.create_session(self.config.nc_user, self.config.nc_password)
            spec = cast(GroupStorageResourceSpecV1, payload.specification)
            te = cast(TargetEntityV1, payload.targetEntity)
            if te.type == "VOListSpecV1":
                vo_list = cast(VOListSpecV1, te.specification)
                json = {
                    "targetEntity": {
                        "type": te.type,
                        "specification": {
                            "vos": vo_list.vos
                        }
                    },
                    "requestType": "ResourceAllocate",
                    "specification": {
                        "desiredName": spec.desiredName,
                        "quota": {
                            "unit": spec.quota.unit,
                            "value": spec.quota.value
                        }
                    },
                    "correlationId": props.correlation_id,
                }
                return self.request_nc_hca("createGroupStorageRequest", json)
            else:
                raise GenericException("TargetEntity type must be 'VOListSpecV1'. Cannot handle other types!")
        else:
            self.logger.error(f"Unknown resource spec '{payload.type}'")
            raise UnknownResourceSpecType(payload.type)

    def resource_update_v1(self, props, payload: ResourceUpdateV1):
        if not payload.type:
            raise ResourceSpecTypeEmpty()
        self.logger.info(f"Update resource of type '{payload.type}'")
        if payload.type == "GroupStorageResourceSpecV1":
            self.create_session(self.config.nc_user, self.config.nc_password)
            spec = cast(GroupStorageResourceSpecV1, payload.specification)
            te = cast(TargetEntityV1, payload.targetEntity)
            if te.type == "VOListSpecV1":
                vo_list = cast(VOListSpecV1, te.specification)
                json = {
                    "targetEntity": {
                        "type": te.type,
                        "specification": {
                            "vos": vo_list.vos
                        }
                    },
                    "requestType": "ResourceUpdate",
                    "specification": {
                        "desiredName": spec.desiredName,
                        "quota": {
                            "unit": spec.quota.unit,
                            "value": spec.quota.value
                        }
                    },
                    "correlationId": props.correlation_id,
                    "resourceId": payload.id,
                }
                return self.request_nc_hca("updateGroupStorageRequest", json)
            else:
                raise GenericException("TargetEntity type must be 'VOListSpecV1'. Cannot handle other types!")
        else:
            self.logger.error(f"Unknown resource spec '{payload.type}'")
            raise UnknownResourceSpecType(payload.type)

    def resource_deallocate_v1(self, props, payload: ResourceDeallocateV1):
        if payload.id:
            self.logger.info(f"Deallocate resource '{payload.id}'")
            self.create_session(self.config.nc_user, self.config.nc_password)
            json = {
                "requestType": "ResourceDeallocate",
                "correlationId": props.correlation_id,
                "resourceId": payload.id,
            }
            return self.request_nc_hca("removeGroupStorageRequest", json)
        else:
            self.logger.error(f"Resource id not set")
            return GenericException("Resource id not set")

    def user_deprovision_v1(self, props, payload: UserDeprovisionV1):
        if payload.id:
            self.logger.info(f"User deprovisioning '{payload.id}'")
            self.create_session(self.config.nc_user, self.config.nc_password)
            json = {
                "requestType": "UserDeprovisioning",
                "correlationId": props.correlation_id,
                "userId": payload.id,
            }
            return self.request_nc_hca("userDeprovisioningRequest", json)
        else:
            self.logger.error(f"User id not set")
            return GenericException("User id not set")
